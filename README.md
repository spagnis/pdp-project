# Programs for computing Travelling Salesperson Problem

Programs are written in C and MPI.

## Genetic algorithms

`genetic_tsp` folder contains the Program for running the Genetic Algorithm.

Running instructions:

Compile the program using make:
`make`
Run it using the following command:
`mpirun -np 2 ./tsp graphs/berlin52.tsp`

Folder Structure:

`tsp.c` is the driver program.
`genesis.c` contains the code related to generation of candidates, mutation, migration and crosser.
`graph.c` contains the code to parse the TSPLIB file and populate objects.
`structs.h` contains all the function declarations and various structs used in the programs.
`graphs` folder has all the graphs I used for running the code.

## Ant Colony Optimization

This program was implemented by Forest Trimble and Scott Todd. And its implementation can be found here:https://github.com/ftrimble/ACO_TSP. I have modified it to make implementations of ACO and Genetic algorithms as close as possible.

Compile the program using make:
`make`

Run it using the following command:
`mpirun -np 2 ./aco_tsp input/berlin52.tsp`

Folder Structure:
All source code is in `src` folder.
`aco_tsp.c` is the driver file for running the code.
`input` has all the TSPLIB graphs used for running this code.
